﻿// <copyright file="IntersectionInformation.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.using System;

namespace Lines
{
    /// <summary>
    /// As each point has more than a position property an intersection point must be sampled from the intersecting segments
    /// </summary>
    public struct IntersectionInformation
    {
        private ContinuousLine continuousLineA;
        private ContinuousLine continuousLineB;
        private float sampleDistanceA;
        private float sampleDistanceB;
        private int segmentStartIndexA;
        private int segmentStartIndexB;
        /// <summary>
        /// Initializes a new instance of the <see cref="IntersectionInformation"/> struct.
        /// </summary>
        /// <param name="continuousLineA">Reference to the First Continuous Line that was tested</param>
        /// <param name="continuousLineB">Reference to the Second Continuous Line that was tested</param>
        /// <param name="segmentStartIndexA">Start Index of the intersecting segment in Continuous Line A</param>
        /// <param name="segmentStartIndexB">Start Index of the intersecting segment in Continuous Line B</param>
        /// <param name="sampleDistanceA">Sample Distance From start of Continuous Line A to the point of intersection, from 0 to 1</param>
        /// <param name="sampleDistanceB">Sample Distance From start of Continuous Line B to the point of intersection, from 0 to 1</param>
        public IntersectionInformation(ContinuousLine continuousLineA, ContinuousLine continuousLineB, int segmentStartIndexA, int segmentStartIndexB, float sampleDistanceA, float sampleDistanceB)
        {
            this.continuousLineA = continuousLineA;
            this.continuousLineB = continuousLineB;
            this.segmentStartIndexA = segmentStartIndexA;
            this.segmentStartIndexB = segmentStartIndexB;
            this.sampleDistanceA = sampleDistanceA;
            this.sampleDistanceB = sampleDistanceB;
        }

        /// <summary>
        /// Gets Continuous Line A
        /// </summary>
        public ContinuousLine ContinuousLineA
        {
            get
            {
                return this.continuousLineA;
            }
        }

        /// <summary>
        /// Gets Continous Line B
        /// </summary>
        public ContinuousLine ContinuousLineB
        {
            get
            {
                return this.continuousLineB;
            }
        }

        /// <summary>
        /// Gets the Sample Distance to the Intersection for Continuous Line A
        /// </summary>
        public float SampleDistanceA
        {
            get
            {
                return this.sampleDistanceA;
            }
        }

        /// <summary>
        /// Gets the Sample Distance to the Intersection for Continuous Line B
        /// </summary>
        public float SampleDistanceB
        {
            get
            {
                return this.sampleDistanceB;
            }
        }

        /// <summary>
        /// Gets Segment Start Index for Continuous Line A
        /// </summary>
        public int SegmentStartIndexA
        {
            get
            {
                return this.segmentStartIndexA;
            }
        }

        /// <summary>
        /// Gets Segment Start Index for Continuous Line B
        /// </summary>
        public int SegmentStartIndexB
        {
            get
            {
                return this.segmentStartIndexB;
            }
        }
    }
}