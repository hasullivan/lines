﻿// <copyright file="Enumerations.cs" company="H.A.Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Group Node</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace Lines
{
    /// <summary>
    /// Enumerations fot he Lines Namespace
    /// </summary>
    public class Enumerations
    {
        /// <summary>
        /// The Method used for smoothing a Coninuous Line
        /// </summary>
        public enum SmoothingMethod
        {
            /// <summary>
            /// Use even averaging between Point and its neighbors
            /// </summary>
            Average,

            /// <summary>
            /// Use a Guassian Kernal to smooth the Point
            /// </summary>
            Gaussian,

            /// <summary>
            /// Use a provided Kernal to smooth the point
            /// </summary>
            Kernal
        }

        /// <summary>
        /// The Type of Delta Reposted for Line Deltas
        /// </summary>
        public enum DeltaType
        {
            /// <summary>
            /// Absolute Delta reports the absolute difference for a delta (always positive)
            /// </summary>
            Absolute,

            /// <summary>
            /// Relative Delta reposts simple difference, where value b is subtracted from value a
            /// </summary>
            Relative
        }
    }
}
