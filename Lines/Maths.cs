﻿// <copyright file="Maths.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Numerics;

namespace Lines
{
    /// <summary>
    /// Internal Maths used for the Lines
    /// </summary>
    internal class Maths
    {
        /// <summary>
        /// Linear Interpolates a float
        /// </summary>
        /// <param name="a">first</param>
        /// <param name="b">second</param>
        /// <param name="t">interpolation point from 0 to 1</param>
        /// <returns>float between a and b at t</returns>
        internal static float Lerp(float a, float b, float t)
        {
            return a + ((b - a) * t);
        }

        /// <summary>
        /// Scales a Vector by a scalar
        /// </summary>
        /// <param name="vector">Vector to scale</param>
        /// <param name="scale">The Scale</param>
        /// <returns>New Scaled Vector</returns>
        internal static Vector3 Scale(Vector3 vector, float scale)
        {
            return vector * scale;
        }

        /// <summary>
        /// Scales a Vector by another Vector
        /// </summary>
        /// <param name="vector">Vector to scale</param>
        /// <param name="scale">Axis Scaling as Vector</param>
        /// <returns>New Scaled Vector</returns>
        internal static Vector3 Scale(Vector3 vector, Vector3 scale)
        {
            return vector * scale;
        }
    }
}
