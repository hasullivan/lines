﻿// <copyright file="Point.cs" company="H.A.Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Numerics;

namespace Lines
{
    /// <summary>
    /// Line Point Class, stores position, normal, color and width information about a given point on a line
    /// </summary>
    public class Point
    {
        private Vector4 color;
        private Vector3 normal;
        private Vector3 position;
        private float width;

        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// </summary>
        /// <param name="position">Position for this Point</param>
        /// <param name="normal">Normal for this point</param>
        /// <param name="color">Color of this point</param>
        /// <param name="width">Width of the line at this point</param>
        public Point(Vector3 position, Vector3 normal, Vector4 color, float width)
        {
            this.position = position;
            this.normal = normal;
            this.color = color;
            this.width = width;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// Copy Constructor
        /// </summary>
        /// <param name="point">New Point That is a copy of the Old Point</param>
        public Point(Point point)
        {
            this.position = point.Position;
            this.normal = point.Normal;
            this.color = point.Color;
            this.width = point.Width;
        }

        /// <summary>
        /// Gets or sets the Color of the line at this point
        /// </summary>
        public Vector4 Color
        {
            get
            {
                return this.color;
            }

            set
            {
                this.color = value;
            }
        }

        /// <summary>
        /// Gets or sets the Normal of the line at this point
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                return this.normal;
            }

            set
            {
                this.normal = value;
            }
        }

        /// <summary>
        /// Gets or sets the Position of the line at this point
        /// </summary>
        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets or sets the Width of line at this point
        /// </summary>
        public float Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.width = value;
            }
        }
    }
}