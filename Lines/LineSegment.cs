﻿// <copyright file="LineSegment.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Numerics;

namespace Lines
{
    /// <summary>
    /// Line Segment Structure
    /// </summary>
    public struct LineSegment
    {
        private Point end;
        private float length;
        private Point start;

        /// <summary>
        /// Initializes a new instance of the <see cref="LineSegment"/> struct.
        /// </summary>
        /// <param name="start">Start point for the Line Segment</param>
        /// <param name="end">End Point for the Line Segment</param>
        public LineSegment(Point start, Point end)
        {
            this.start = start;
            this.end = end;
            this.length = Vector3.Distance(this.start.Position, this.end.Position);
        }

        /// <summary>
        /// Gets the End Point for the Line Segment
        /// </summary>
        public Point End
        {
            get
            {
                return this.End;
            }
        }

        /// <summary>
        /// Gets the Length of this Line Segment
        /// </summary>
        public float Length
        {
            get
            {
                return this.length;
            }
        }

        /// <summary>
        /// Gets the Start Point for the Line Segment
        /// </summary>
        public Point Start
        {
            get
            {
                return this.start;
            }
        }

        /// <summary>
        /// Calculates the Delta values of two line segments
        /// </summary>
        /// <param name="a">First Line Segment</param>
        /// <param name="b">Second Line Segment</param>
        /// <param name="deltaType">Delta Type</param>
        /// <returns>New Line Segment</returns>
        public static LineSegment CalculateDeltaOfSegments(LineSegment a, LineSegment b, Enumerations.DeltaType deltaType)
        {
            Vector3 positionStart = Vector3.Zero;
            Vector3 normalStart = Vector3.Zero;
            Vector4 colorStart = Vector4.Zero;
            float widthStart = 0.0f;

            Vector3 positionEnd = Vector3.Zero;
            Vector3 normalEnd = Vector3.Zero;
            Vector4 colorEnd = Vector4.Zero;
            float widthEnd = 0.0f;

            switch (deltaType)
            {
                case Enumerations.DeltaType.Absolute:
                    positionStart = Vector3.Abs(a.start.Position - b.start.Position);
                    positionEnd = Vector3.Abs(a.end.Position = b.end.Position);
                    normalStart = Vector3.Abs(a.start.Normal - b.start.Normal);
                    normalEnd = Vector3.Abs(a.end.Normal - b.end.Normal);
                    colorStart = Vector4.Abs(a.start.Color - b.start.Color);
                    colorEnd = Vector4.Abs(a.end.Color - b.end.Color);
                    widthStart = Math.Abs(a.start.Width - b.start.Width);
                    widthEnd = Math.Abs(a.end.Width - b.end.Width);
                    break;

                case Enumerations.DeltaType.Relative:
                    positionStart = a.start.Position - b.start.Position;
                    positionEnd = a.end.Position = b.end.Position;
                    normalStart = a.start.Normal - b.start.Normal;
                    normalEnd = a.end.Normal - b.end.Normal;
                    colorStart = a.start.Color - b.start.Color;
                    colorEnd = a.end.Color - b.end.Color;
                    widthStart = a.start.Width - b.start.Width;
                    widthEnd = a.end.Width - b.end.Width;
                    break;
            }

            return new LineSegment(new Point(positionStart, normalStart, colorStart, widthStart), new Point(positionEnd, normalEnd, colorEnd, widthEnd));
        }

        /// <summary>
        /// Calulates the Normals for the start and end point
        /// </summary>
        public void CalculateNormals()
        {
            this.start.Normal = Vector3.Normalize(Vector3.Cross(this.end.Position - this.start.Position, this.start.Position - this.end.Position));
            this.end.Normal = Vector3.Normalize(Vector3.Cross(this.start.Position - this.end.Position, this.end.Position - this.start.Position));
        }

        /// <summary>
        /// Checks if two line segments intersect
        /// </summary>
        /// <param name="lineSegment">Line Segment to test against</param>
        /// <param name="segmentIntersectionInformation">Information about the intersection on the two Line Segments</param>
        /// <returns>Bool</returns>
        public bool DoSegmentsIntersect(LineSegment lineSegment, out SegmentIntersectionInformation segmentIntersectionInformation)
        {
            // Early Out Bounds Check
            SegmentSphericalBounds segmentABounds = new SegmentSphericalBounds(this.start.Position, this.end.Position);
            SegmentSphericalBounds segmentBBounds = new SegmentSphericalBounds(lineSegment.Start.Position, lineSegment.End.Position);

            if (segmentABounds.DoesIntersectWith(segmentBBounds) == false)
            {
                segmentIntersectionInformation = new SegmentIntersectionInformation(this, lineSegment, 0.0f, 0.0f);
                return false;
            }

            bool doSegmentsIntersect = false;
            Vector3 directionA = Vector3.Normalize(this.end.Position - this.start.Position);
            Vector3 directionB = Vector3.Normalize(lineSegment.End.Position - lineSegment.start.Position);

            Vector3 directionAB = lineSegment.Start.Position - this.Start.Position;
            Vector3 crossDirections = Vector3.Cross(directionA, directionB);
            float crossDirectionMagnitude = crossDirections.LengthSquared();
            float dotDirectionABCrossAB = Math.Abs(Vector3.Dot(directionAB, crossDirections));

            Vector3 intersectionPosition;

            if (dotDirectionABCrossAB < 0.00001f && crossDirectionMagnitude > 0.00001f)
            {
                Vector3 crossDirectionABDirectionB = Vector3.Cross(directionAB, directionB);

                float s = Vector3.Dot(crossDirectionABDirectionB, crossDirections) / crossDirectionMagnitude;
                intersectionPosition = this.start.Position + (directionA * s);

                float segmentADistanceToIntersection = Vector3.Distance(this.start.Position, intersectionPosition);
                float segmentBDistanceToIntersection = Vector3.Distance(lineSegment.Start.Position, intersectionPosition);

                if (this.Length >= segmentADistanceToIntersection && lineSegment.Length >= segmentBDistanceToIntersection)
                {
                    float segmentASampleDistance = segmentADistanceToIntersection / this.Length;
                    float segmentBSampleDistance = segmentBDistanceToIntersection / lineSegment.Length;

                    segmentIntersectionInformation = new SegmentIntersectionInformation(this, lineSegment, segmentASampleDistance, segmentBSampleDistance);
                    doSegmentsIntersect = true;
                }
                else
                {
                    segmentIntersectionInformation = new SegmentIntersectionInformation(this, lineSegment, 0.0f, 0.0f);
                }
            }
            else
            {
                segmentIntersectionInformation = new SegmentIntersectionInformation(this, lineSegment, 0.0f, 0.0f);
            }

            return doSegmentsIntersect;
        }

        /// <summary>
        /// Returns normalized Direction from start position to end position
        /// </summary>
        /// <returns>Normalized Vector3</returns>
        public Vector3 GetDirection()
        {
            return Vector3.Normalize(this.end.Position - this.start.Position);
        }

        /// <summary>
        /// Samples a point along the Line Segment
        /// </summary>
        /// <param name="t">Sample distance along the Line segment, from 0..1</param>
        /// <returns>New Point sampled along the line segment</returns>
        public Point SamplePointOnLine(float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;

            Vector3 samplePosition = Vector3.Lerp(this.start.Position, this.end.Position, clampedT);
            Vector3 sampleNormal = Vector3.Normalize(Vector3.Lerp(this.start.Normal, this.end.Normal, clampedT));
            Vector4 sampleColor = Vector4.Lerp(this.start.Color, this.end.Color, clampedT);
            float sampleWidth = Maths.Lerp(this.start.Width, this.end.Width, clampedT);

            return new Point(samplePosition, sampleNormal, sampleColor, sampleWidth);
        }
    }
}