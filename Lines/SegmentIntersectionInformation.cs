﻿// <copyright file="SegmentIntersectionInformation.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace Lines
{
    /// <summary>
    /// Line Segment Intersection Information
    /// </summary>
    public struct SegmentIntersectionInformation
    {
        private float sampleDistanceA;
        private float sampleDistanceB;
        private LineSegment segmentA;
        private LineSegment segmentB;

        /// <summary>
        /// Initializes a new instance of the <see cref="SegmentIntersectionInformation"/> struct.
        /// </summary>
        /// <param name="segmentA">Reference to first Line Segment</param>
        /// <param name="segmentB">Reference to second Line Segment</param>
        /// <param name="sampleDistanceA">Distance betwen start and end point where intersection occured, between 0..1, for segment A</param>
        /// <param name="sampleDistanceB">Distance betwen start and end point where intersection occured, between 0..1, for segment B</param>
        public SegmentIntersectionInformation(LineSegment segmentA, LineSegment segmentB, float sampleDistanceA, float sampleDistanceB)
        {
            this.segmentA = segmentA;
            this.segmentB = segmentB;
            this.sampleDistanceA = sampleDistanceA;
            this.sampleDistanceB = sampleDistanceB;
        }

        /// <summary>
        /// Gets the Sample Distance for intersection on Line Segment A
        /// </summary>
        public float SampleDistanceA
        {
            get
            {
                return this.sampleDistanceA;
            }
        }

        /// <summary>
        /// Gets the Sample Distance for intersection on Line Segment B
        /// </summary>
        public float SampleDistanceB
        {
            get
            {
                return this.sampleDistanceB;
            }
        }

        /// <summary>
        /// Gets the reference to Line Segment A
        /// </summary>
        public LineSegment SegmentA
        {
            get
            {
                return this.segmentA;
            }
        }

        /// <summary>
        /// Gets the reference to Line Segment B
        /// </summary>
        public LineSegment SegmentB
        {
            get
            {
                return this.segmentB;
            }
        }
    }
}
