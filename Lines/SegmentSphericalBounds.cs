﻿// <copyright file="SegmentSphericalBounds.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
using System.Numerics;

namespace Lines
{
    /// <summary>
    /// Sphereical Bound for a Line Segments
    /// </summary>
    internal struct SegmentSphericalBounds
    {
        private Vector3 centerPosition;
        private float radius;

        /// <summary>
        /// Initializes a new instance of the <see cref="SegmentSphericalBounds"/> struct.
        /// </summary>
        /// <param name="positionA">Start Position of the Line Segment</param>
        /// <param name="positionB">End Position of the Line Segment</param>
        public SegmentSphericalBounds(Vector3 positionA, Vector3 positionB)
        {
            this.centerPosition = (positionA + positionB) * .5f;
            this.radius = Vector3.Distance(positionA, positionB) * .5f;
        }

        /// <summary>
        /// Gets the Center Position
        /// </summary>
        public Vector3 CenterPosition
        {
            get
            {
                return this.centerPosition;
            }
        }

        /// <summary>
        /// Gets the radius
        /// </summary>
        public float Radius
        {
            get
            {
                return this.radius;
            }
        }

        /// <summary>
        /// Checks to see if another bounding sphere intersects with this one
        /// </summary>
        /// <param name="other">The Other Sperical Bounds</param>
        /// <returns>True if Bounds intersect</returns>
        public bool DoesIntersectWith(SegmentSphericalBounds other)
        {
            bool doesIntersect = false;
            float centerDistance = Vector3.Distance(this.CenterPosition, other.CenterPosition);
            float radii = this.Radius + other.Radius;

            if (centerDistance < radii)
            {
                doesIntersect = true;
            }

            return doesIntersect;
        }
    }
}
