﻿// <copyright file="ContinuousLine.cs" company="H.A. Sullivan">
// Copyright (c) H.A. Sullivan. All rights reserved.
// </copyright>
// <author>H.A. Sullivan</author>
// <date>05/08/2016  </date>
// <summary>Continuous Line</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Numerics;

namespace Lines
{
    /// <summary>
    /// Continuous Line is a List of connected Line Segments
    /// It is implimented in 3D but can just as easily be used in 2d by setting z to 0
    /// For the purpose of transformations it is assumed the Points are right handed
    /// </summary>
    public class ContinuousLine
    {
        private List<Point> points = new List<Point>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ContinuousLine"/> class.
        /// </summary>
        public ContinuousLine()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContinuousLine"/> class.
        /// </summary>
        /// <param name="points">List of points that make up the Continuous Line</param>
        public ContinuousLine(List<Point> points)
        {
            this.Points = points;
        }

        /// <summary>
        /// Gets the Number of Points in the Continuous Line
        /// </summary>
        /// <returns>Number of Points</returns>
        public int Count
        {
            get
            {
                return this.Points.Count;
            }
        }

        /// <summary>
        /// Gets the Length of the Continuous Line
        /// </summary>
        /// <returns>Length as float</returns>
        public float Length
        {
            get
            {
                float lineLength = 0.0f;

                for (int i = 0; i < this.Points.Count - 1; i++)
                {
                    lineLength += Vector3.Distance(this.Points[i].Position, this.Points[i + 1].Position);
                }

                return lineLength;
            }
        }

        /// <summary>
        /// Gets or sets the Points
        /// </summary>
        public List<Point> Points
        {
            get
            {
                return this.points;
            }

            set
            {
                this.points = value;
            }
        }

        /// <summary>
        /// Calcualtes delta values from point to point in the provided line
        /// </summary>
        /// <param name="continuousLine">Continuous Line</param>
        /// <param name="deltaType">Type of delta to report</param>
        /// <returns>New Continuous Line</returns>
        public static ContinuousLine CalculatePointToPointDeltas(ContinuousLine continuousLine, Enumerations.DeltaType deltaType)
        {
            ContinuousLine deltaContinuousLine = new ContinuousLine();

            switch (deltaType)
            {
                case Enumerations.DeltaType.Absolute:
                    for (int i = 0; i < continuousLine.points.Count; i++)
                    {
                        if (i != 0)
                        {
                            Vector3 deltaPosition = Vector3.Abs(continuousLine.points[i].Position - continuousLine.points[i - 1].Position);
                            Vector3 deltaNormal = Vector3.Abs(continuousLine.points[i].Normal - continuousLine.points[i - 1].Normal);
                            Vector4 deltaColor = Vector4.Abs(continuousLine.points[i].Color - continuousLine.points[i - 1].Color);
                            float deltaWidth = Math.Abs(continuousLine.points[i].Width - continuousLine.points[i - 1].Width);
                            deltaContinuousLine.AddPoint(deltaPosition, deltaNormal, deltaColor, deltaWidth);
                        }
                    }

                    break;

                case Enumerations.DeltaType.Relative:
                    for (int i = 0; i < continuousLine.points.Count; i++)
                    {
                        if (i != 0)
                        {
                            Vector3 deltaPosition = continuousLine.points[i].Position - continuousLine.points[i - 1].Position;
                            Vector3 deltaNormal = continuousLine.points[i].Normal - continuousLine.points[i - 1].Normal;
                            Vector4 deltaColor = continuousLine.points[i].Color - continuousLine.points[i - 1].Color;
                            float deltaWidth = continuousLine.points[i].Width - continuousLine.points[i - 1].Width;
                            deltaContinuousLine.AddPoint(deltaPosition, deltaNormal, deltaColor, deltaWidth);
                        }
                    }

                    break;
            }

            return deltaContinuousLine;
        }

        /// <summary>
        /// Creates a New Continuous Line by Merging Two Continuous Lines
        /// </summary>
        /// <param name="first">First Continuous Line</param>
        /// <param name="second">Second Continuous Line</param>
        /// <returns>New Continuous Line</returns>
        public static ContinuousLine MergeLines(ContinuousLine first, ContinuousLine second)
        {
            ContinuousLine mergedLine = first;
            first.Points.AddRange(second.Points);
            return mergedLine;
        }

        /// <summary>
        /// Splits the Continuous Line into two seperate Lines
        /// </summary>
        /// <param name="line">The Line to Split</param>
        /// <param name="index">Index to split</param>
        /// <returns>Array of New Continuous Lines</returns>
        public static ContinuousLine[] SplitLine(ContinuousLine line, int index)
        {
            ContinuousLine[] lines = new ContinuousLine[2];
            for (int i = 0; i < index + 1; i++)
            {
                lines[0].AddPoint(line.Points[i]);
            }

            for (int i = index; i < line.Count; i++)
            {
                lines[1].AddPoint(line.Points[i]);
            }

            return lines;
        }

        /// <summary>
        /// Adds a new Point to the end of the Coninuous Line
        /// </summary>
        /// <param name="position">Point to add</param>
        /// /// <param name="normal">Normal for the added Point</param>
        /// <param name="color">Color for the added Point</param>
        /// <param name="width"> With of the Point</param>
        public void AddPoint(Vector3 position, Vector3 normal, Vector4 color, float width)
        {
            this.Points.Add(new Point(position, normal, color, width));
        }

        /// <summary>
        /// Adds a new Point to the end of a Continuous Line
        /// </summary>
        /// <param name="point">Point to add to this Continuous Line</param>
        public void AddPoint(Point point)
        {
            this.Points.Add(point);
        }

        /// <summary>
        /// Appends a Continuous Line to the End of this Continuous Line
        /// </summary>
        /// <param name="line">Continuous Line to Append</param>
        public void AppendLine(ContinuousLine line)
        {
            this.Points.AddRange(line.Points);
        }

        /// <summary>
        /// Calculates the Point Normals for each Point on the line
        /// The Normal is defined as the direction this point faces relative to its neighbors
        /// </summary>
        public void CalculateNormals()
        {
            for (int i = 0; i < this.points.Count; i++)
            {
                if (i == 0)
                {
                    Vector3 a = this.points[i + 1].Position - this.points[i].Position;
                    Vector3 b = this.points[i].Position - this.points[i + 1].Position;
                    this.points[i].Normal = Vector3.Normalize(Vector3.Cross(a, b));
                }
                else if (i == this.points.Count - 1)
                {
                    Vector3 a = this.points[i].Position - this.points[i - 1].Position;
                    Vector3 b = this.points[i - 1].Position - this.points[i].Position;
                    this.points[i].Normal = Vector3.Normalize(Vector3.Cross(a, b));
                }
                else
                {
                    Vector3 a = this.points[i].Position - this.points[i - 1].Position;
                    Vector3 b = this.points[i].Position - this.points[i + 1].Position;
                    this.points[i].Normal = Vector3.Normalize(Vector3.Cross(a, b));
                }
            }
        }

        /// <summary>
        /// Clears all points in the Continuous Line
        /// </summary>
        public void ClearLine()
        {
            this.Points.Clear();
        }

        /// <summary>
        /// Does this Continuous Line intersect with another Continuous Line
        /// </summary>
        /// <param name="continuousLine">The other Continuous Line</param>
        /// <returns>True if there is any intersections</returns>
        public bool DoesIntersectWith(ContinuousLine continuousLine)
        {
            bool intersects = false;

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = 0; j < continuousLine.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    bool doesIntersect = false;
                    doesIntersect = this.DoSegmentsIntersect(this, continuousLine, i, j, out intersectionInformation);

                    if (doesIntersect == true)
                    {
                        return true;
                    }
                }
            }

            return intersects;
        }

        /// <summary>
        /// Checks if this Continuous Line intersect with itself
        /// </summary>
        /// <returns>True if any segment intersect with any other segment</returns>
        public bool DoesSelfIntersect()
        {
            bool doesSelfIntersect = false;

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = i + 2; j < this.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    doesSelfIntersect = this.DoSegmentsIntersect(this, this, i, j, out intersectionInformation);

                    if (doesSelfIntersect == true)
                    {
                        return true;
                    }
                }
            }

            return doesSelfIntersect;
        }

        /// <summary>
        /// Gets the Color at the Index
        /// </summary>
        /// <param name="index">Index of the Color</param>
        /// <returns>Color as Vector4</returns>
        public Vector4 GetColor(int index)
        {
            return this.Points[index].Color;
        }

        /// <summary>
        /// Gets the Direction of the Line at the given point
        /// Direction is calculated as the vector from the index to the next index
        /// </summary>
        /// <param name="index">Index of Point</param>
        /// <returns>Vector3</returns>
        public Vector3 GetDirection(int index)
        {
            if (index == this.Points.Count - 1)
            {
                return Vector3.Zero;
            }
            else
            {
                return Vector3.Normalize(this.Points[index + 1].Position - this.Points[index].Position);
            }
        }

        /// <summary>
        /// Gets a list of new Sampled points at every intersection between the two Continuous Lines
        /// </summary>
        /// <param name="continuousLine">Continuous Line to test against</param>
        /// <returns>List of Points sampled from intersections</returns>
        public List<Point> GetLineIntersectionPoints(ContinuousLine continuousLine)
        {
            List<Point> intersectionPoints = new List<Point>();

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = 0; j < continuousLine.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    bool doesIntersect = false;
                    doesIntersect = this.DoSegmentsIntersect(this, continuousLine, i, j, out intersectionInformation);

                    if (doesIntersect == true)
                    {
                        Point pointA = intersectionInformation.ContinuousLineA.SamplePoint(intersectionInformation.SampleDistanceA);
                        Point pointB = intersectionInformation.ContinuousLineB.SamplePoint(intersectionInformation.SampleDistanceB);
                        Point sampledPoint = new Point(
                            pointA.Position,
                            Vector3.Normalize((pointA.Normal + pointB.Normal) * .5f),
                            (pointA.Color + pointB.Color) * .5f,
                            (pointA.Width + pointB.Width) * .5f);
                        intersectionPoints.Add(sampledPoint);
                    }
                }
            }

            return intersectionPoints;
        }

        /// <summary>
        /// Gets a list of Intersection Information at Every Intersection Between the Two Lines
        /// </summary>
        /// <param name="continuousLine">Continuous Line to test against</param>
        /// <returns>List of Intersection Information</returns>
        public List<IntersectionInformation> GetLineIntersections(ContinuousLine continuousLine)
        {
            List<IntersectionInformation> intersections = new List<IntersectionInformation>();

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = 0; j < continuousLine.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    bool doesIntersect = false;
                    doesIntersect = this.DoSegmentsIntersect(this, continuousLine, i, j, out intersectionInformation);

                    if (doesIntersect == true)
                    {
                        intersections.Add(intersectionInformation);
                    }
                }
            }

            return intersections;
        }

        /// <summary>
        /// Gets the Normal of the Point at Index
        /// </summary>
        /// <param name="index">Point to get Normal from</param>
        /// <returns>Normal as Vector3</returns>
        public Vector3 GetNormal(int index)
        {
            return this.Points[index].Normal;
        }

        /// <summary>
        /// Gets the Vector if point at Index
        /// </summary>
        /// <param name="index">Index of Point</param>
        /// <returns>Point at Index</returns>
        public Point GetPoint(int index)
        {
            return this.Points[index];
        }

        /// <summary>
        /// Gets the Position of the point at Index
        /// </summary>
        /// <param name="index">Point to get position from</param>
        /// <returns>Position</returns>
        public Vector3 GetPosition(int index)
        {
            return this.Points[index].Position;
        }

        /// <summary>
        /// Gets The linear interpolated Color half way between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <returns>Interpolated Color</returns>
        public Vector4 GetSegmentMidColor(int a, int b)
        {
            return Vector4.Lerp(this.points[a].Color, this.points[b].Color, .5f);
        }

        /// <summary>
        /// Gets The linear interpolated Color half way between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <returns>Interpolated Normal</returns>
        public Vector3 GetSegmentMidNormal(int a, int b)
        {
            return Vector3.Normalize(Vector3.Lerp(this.points[a].Normal, this.points[b].Normal, .5f));
        }

        /// <summary>
        /// Gets The linear interpolated Point half way between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <returns>Interpolated Point</returns>
        public Point GetSegmentMidPoint(int a, int b)
        {
            Vector3 midPosition = Vector3.Lerp(this.points[a].Position, this.points[b].Position, .5f);
            Vector3 midNormal = Vector3.Normalize(Vector3.Lerp(this.points[a].Normal, this.points[b].Normal, .5f));
            Vector4 midColor = Vector4.Lerp(this.points[a].Color, this.points[b].Color, .5f);
            float midWidth = Maths.Lerp(this.points[a].Width, this.points[b].Width, .5f);

            return new Point(midPosition, midNormal, midColor, midWidth);
        }

        /// <summary>
        /// Gets a list of New Sampled Points at every self intersection
        /// </summary>
        /// <returns>List of intersections as Points</returns>
        public List<Point> GetSelfIntersectionPoints()
        {
            List<Point> intersectionPoints = new List<Point>();

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = i + 2; j < this.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    bool doesSelfIntersect = false;
                    doesSelfIntersect = this.DoSegmentsIntersect(this, this, i, j, out intersectionInformation);

                    if (doesSelfIntersect == true)
                    {
                        Point pointA = intersectionInformation.ContinuousLineA.SamplePoint(intersectionInformation.SampleDistanceA);
                        Point pointB = intersectionInformation.ContinuousLineB.SamplePoint(intersectionInformation.SampleDistanceB);
                        Point sampledPoint = new Point(
                            pointA.Position,
                            Vector3.Normalize((pointA.Normal + pointB.Normal) * .5f),
                            (pointA.Color + pointB.Color) * .5f,
                            (pointA.Width + pointB.Width) * .5f);
                        intersectionPoints.Add(sampledPoint);
                    }
                }
            }

            return intersectionPoints;
        }

        /// <summary>
        /// Gets a list of Intersection Information at every self intersection
        /// </summary>
        /// <returns>List of intersections as Points</returns>
        public List<IntersectionInformation> GetSelfIntersections()
        {
            List<IntersectionInformation> intersections = new List<IntersectionInformation>();

            for (int i = 0; i < this.points.Count - 1; i++)
            {
                for (int j = i + 2; j < this.points.Count - 1; j++)
                {
                    IntersectionInformation intersectionInformation;
                    bool doesSelfIntersect = false;
                    doesSelfIntersect = this.DoSegmentsIntersect(this, this, i, j, out intersectionInformation);

                    if (doesSelfIntersect == true)
                    {
                        intersections.Add(intersectionInformation);
                    }
                }
            }

            return intersections;
        }

        /// <summary>
        /// Gets the Width of the Point at Index
        /// </summary>
        /// <param name="index">Point to get the Width from</param>
        /// <returns>Width at Index point</returns>
        public float GetWidth(int index)
        {
            return this.Points[index].Width;
        }

        /// <summary>
        /// Inserts a new Point in the Continuous Line
        /// </summary>
        /// <param name="index">The Index where the new Point will be placed</param>
        /// <param name="position">The Point to add</param>
        /// <param name="color">The Color of the new Point</param>
        /// <param name="normal">The Normal of the new Point</param>
        /// <param name="width">With of the point being inserted</param>
        public void InsertPoint(int index, Vector3 position, Vector4 color, Vector3 normal, float width)
        {
            this.Points.Insert(index, new Point(position, normal, color, width));
        }

        /// <summary>
        /// Inserts a new Point into the Coninuous Line
        /// </summary>
        /// <param name="index">Index Where the new Point will be placed</param>
        /// <param name="point">Point to insert</param>
        public void InsertPoint(int index, Point point)
        {
            this.Points.Insert(index, point);
        }

        /// <summary>
        /// Samples the Point on the Continuous Line between 0 to 1.0
        /// Inserts a new point in the Continuous Line into the segment that was sampled
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        public void InsertPointOnLine(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;
            Vector3 samplePosition;
            Vector3 sampleNormal;
            Vector4 sampleColor;
            float sampleWidth;
            Point newPoint;

            // it would be faster to accumulate lengths
            // Get the lengths and ratios for each index
            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));

            samplePosition = Vector3.Lerp(this.points[rangeA].Position, this.points[rangeB].Position, lerpValue);
            sampleNormal = Vector3.Normalize(Vector3.Lerp(this.points[rangeA].Normal, this.points[rangeB].Normal, lerpValue));
            sampleColor = Vector4.Lerp(this.points[rangeA].Color, this.points[rangeB].Color, lerpValue);
            sampleWidth = Maths.Lerp(this.points[rangeA].Width, this.points[rangeB].Width, lerpValue);

            newPoint = new Point(samplePosition, sampleNormal, sampleColor, sampleWidth);
            this.points.Insert(rangeB, newPoint);
        }

        /// <summary>
        /// Inverts all of the Point Normals in the Continuous Line
        /// </summary>
        public void InvertNormals()
        {
            for (int i = 0; i < this.points.Count; i++)
            {
                this.points[i].Normal = this.points[i].Normal * -1.0f;
            }
        }

        /// <summary>
        /// Gets the Length of the Continuous Line up to the Point at Index
        /// </summary>
        /// <param name="index">Index of Point</param>
        /// <returns>Length as float</returns>
        public float LengthToPoint(int index)
        {
            float length = 0.0f;

            for (int i = 0; i < index; i++)
            {
                if (i != 0)
                {
                    length += Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position);
                }
            }

            return length;
        }

        /// <summary>
        /// Normalize the Normals in all of the Point in this Continuous Line
        /// </summary>
        public void Normalize()
        {
            for (int i = 0; i < this.Points.Count; i++)
            {
                this.Points[i].Normal = Vector3.Normalize(this.Points[i].Normal);
            }
        }

        /// <summary>
        /// Prepends a Continuous Line to the Beginning of the Continuous Line
        /// </summary>
        /// <param name="line">Continuous Lineto Prepend</param>
        public void PrependLine(ContinuousLine line)
        {
            this.points.InsertRange(0, line.points);
        }

        /// <summary>
        /// Redistribute points will evenly resample the Coninuous Line by number of points.
        /// Then Points will be set to the sampled points so that the distance between each point is the same
        /// </summary>
        public void RedistributePoints()
        {
            List<Point> newPoints = new List<Point>();
            float increment = 1.0f / (this.points.Count - 1);

            for (int i = 0; i < this.points.Count; i++)
            {
                if (i != 0 && i != this.points.Count - 1)
                {
                    newPoints.Add(this.SamplePoint(increment * i));
                }
                else
                {
                    newPoints.Add(this.points[i]);
                }
            }

            this.points = newPoints;
        }

        /// <summary>
        /// Removes a Point from the Continuous Line
        /// Will also remove the Points Color and Normal
        /// </summary>
        /// <param name="index">Index of the Point to remove</param>
        public void RemovePoint(int index)
        {
            if (index < this.points.Count && index >= 0)
            {
                this.Points.RemoveAt(index);
            }
        }

        /// <summary>
        /// Resample Line will Sample the continuous line at num samples
        /// It will then create a new continuous line at these sample points
        /// </summary>
        /// <param name="samples">Number of samples to take</param>
        public void ResampleLine(int samples)
        {
            List<Point> newPoints = new List<Point>();
            float increment = 1.0f / (samples - 1.0f);

            // TODO: Reorder this
            for (int i = 0; i < samples; i++)
            {
                if (i == 0)
                {
                    newPoints.Add(this.points[0]);
                }
                else if (i == samples - 1)
                {
                    newPoints.Add(this.points[this.points.Count - 1]);
                }
                else
                {
                    newPoints.Add(this.SamplePoint(increment * i));
                }
            }

            this.points = newPoints;
        }

        /// <summary>
        /// Samples a Color along the Continuous Line from 0 to 1.0
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        /// <returns>Sampled Color</returns>
        public Vector4 SampleColor(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;

            // it would be faster to accumulate lengths
            // Get the lengths and ratios for each index
            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));
            return Vector4.Lerp(this.points[rangeA].Color, this.points[rangeB].Color, lerpValue);
        }

        /// <summary>
        /// Samples a Width along the Continuous Line from 0 to 1.0
        /// This would give the same result as sampling the direction of an index
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        /// <returns>Sampled Direction</returns>
        public Vector3 SampleDirection(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;

            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    break;
                }
            }

            return this.GetDirection(rangeA);
        }

        /// <summary>
        /// Samples a Normal along the Continuous Line from 0 to 1.0
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        /// <returns>Sampled Normal</returns>
        public Vector3 SampleNormal(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;

            // it would be faster to accumulate lengths
            // Get the lengths and ratios for each index
            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));
            return Vector3.Normalize(Vector3.Lerp(this.points[rangeA].Normal, this.points[rangeB].Normal, lerpValue));
        }

        /// <summary>
        /// Sample this Continuous Line at a position Between 0 and 1.0
        /// </summary>
        /// <param name="position">Float from 0 to 1.0 (clamped) where to sample this Continuous Line</param>
        /// <returns>New Point Sampled at position in line</returns>
        public Point SamplePoint(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;
            Vector3 samplePosition;
            Vector3 sampleNormal;
            Vector4 sampleColor;
            float sampleWidth;

            // it would be faster to accumulate lengths
            // Get the lengths and ratios for each index
            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition >= linePercent[i] && clampedPosition <= linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));

            samplePosition = Vector3.Lerp(this.points[rangeA].Position, this.points[rangeB].Position, lerpValue);
            sampleNormal = Vector3.Normalize(Vector3.Lerp(this.points[rangeA].Normal, this.points[rangeB].Normal, lerpValue));
            sampleColor = Vector4.Lerp(this.points[rangeA].Color, this.points[rangeB].Color, lerpValue);
            sampleWidth = Maths.Lerp(this.points[rangeA].Width, this.points[rangeB].Width, lerpValue);

            return new Point(samplePosition, sampleNormal, sampleColor, sampleWidth);
        }

        /// <summary>
        /// Samples the Position along the Continuous Line in range of 0 to 1.0
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        /// <returns>Sampled Position</returns>
        public Vector3 SamplePosition(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;

            // it would be faster to accumulate lengths
            // Get the lengths and ratios for each index
            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    // sum up instead of calling getlengthatpoint and doing same calc over and over
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            // find range points
            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));
            return Vector3.Lerp(this.points[rangeA].Position, this.points[rangeB].Position, lerpValue);
        }

        /// <summary>
        /// Gets The linear interpolated Color between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <param name="t">0 to 1.0</param>
        /// <returns>Sampled Color</returns>
        public Vector4 SampleSegmentColor(int a, int b, float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;
            return Vector4.Lerp(this.points[a].Color, this.points[b].Color, clampedT);
        }

        /// <summary>
        /// Gets The linear interpolated Normal between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <param name="t">0 to 1.0</param>
        /// <returns>Sampled Normal</returns>
        public Vector3 SampleSegmentNormal(int a, int b, float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;
            return Vector3.Normalize(Vector3.Lerp(this.points[a].Normal, this.points[b].Normal, clampedT));
        }

        /// <summary>
        /// Gets The linear interpolated Point between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <param name="t">0 to 1.0</param>
        /// <returns>Sampled Point</returns>
        public Point SampleSegmentPoint(int a, int b, float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;

            Vector3 samplePosition = Vector3.Lerp(this.points[a].Position, this.points[b].Position, clampedT);
            Vector3 sampleNormal = Vector3.Normalize(Vector3.Lerp(this.points[a].Normal, this.points[b].Normal, clampedT));
            Vector4 sampleColor = Vector4.Lerp(this.points[a].Color, this.points[b].Color, clampedT);
            float sampleWidth = Maths.Lerp(this.points[a].Width, this.points[b].Width, clampedT);

            return new Point(samplePosition, sampleNormal, sampleColor, sampleWidth);
        }

        /// <summary>
        /// Gets The linear interpolated Position between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <param name="t">0 to 1.0</param>
        /// <returns>Sampled Position</returns>
        public Vector3 SampleSegmentPosition(int a, int b, float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;
            return Vector3.Lerp(this.points[a].Position, this.points[b].Position, clampedT);
        }

        /// <summary>
        /// Gets The linear interpolated width between two points
        /// </summary>
        /// <param name="a">Index of first Point</param>
        /// <param name="b">Index of second Point</param>
        /// <param name="t">0 to 1.0</param>
        /// <returns>Sampled Width</returns>
        public float SampleSegmentWidth(int a, int b, float t)
        {
            float clampedT = (t < 0.0f) ? 0.0f : (t > 1.0f) ? 1.0f : t;
            return Maths.Lerp(this.points[a].Width, this.points[b].Width, clampedT);
        }

        /// <summary>
        /// Samples a Width along the Continuous Line from 0 to 1.0
        /// </summary>
        /// <param name="position">0 to 1.0</param>
        /// <returns>Sampled width</returns>
        public float SampleWidth(float position)
        {
            float clampedPosition = (position < 0.0f) ? 0.0f : (position > 1.0f) ? 1.0f : position;
            float lineLength = this.Length;
            float[] lengths = new float[this.Points.Count];
            float[] linePercent = new float[this.Points.Count];
            int rangeA = 0;
            int rangeB = 0;

            lengths[0] = 0.0f;
            for (int i = 0; i < lengths.Length; i++)
            {
                if (i != 0)
                {
                    lengths[i] = Vector3.Distance(this.Points[i].Position, this.Points[i - 1].Position) + lengths[i - 1];
                }

                linePercent[i] = lengths[i] / lineLength;
            }

            for (int i = 0; i < linePercent.Length - 1; i++)
            {
                if (clampedPosition > linePercent[i] && clampedPosition < linePercent[i + 1])
                {
                    rangeA = i;
                    rangeB = i + 1;
                    break;
                }
            }

            float lerpValue = (position - linePercent[rangeA]) * (1.0f / (linePercent[rangeB] - linePercent[rangeA]));
            return Maths.Lerp(this.points[rangeA].Width, this.points[rangeB].Width, lerpValue);
        }

        /// <summary>
        /// Scales the Positions for each point in the Continuous Line
        /// </summary>
        /// <param name="scale">Scales for each axis</param>
        public void Scale(Vector3 scale)
        {
            for (int i = 0; i < this.points.Count; i++)
            {
                this.Points[i].Position = Maths.Scale(this.Points[i].Position, scale);
            }
        }

        /// <summary>
        /// Sets the Values of a Point
        /// </summary>
        /// <param name="index">Index of the Point that will be set</param>
        /// <param name="position">New Point value</param>
        /// <param name="normal">New Point Normal Value</param>
        /// /// <param name="color">New Point Color Value</param>
        /// <param name="width">New Width for this Point</param>
        public void SetPoint(int index, Vector3 position, Vector3 normal, Vector4 color, float width)
        {
            this.Points[index].Position = position;
            this.Points[index].Normal = normal;
            this.Points[index].Color = color;
            this.Points[index].Width = width;
        }

        /// <summary>
        /// Smooths Only the Colors of the Continuous Lines useing Averaging
        /// </summary>
        /// <param name="iterations">Number of Times to loops the Continuous line and smooth the Colors</param>
        /// <param name="includeEndpoints">true is the first and last color should also be smoothed</param>
        public void SmoothColorsUsingAverage(int iterations, bool includeEndpoints)
        {
            for (int i = 0; i < iterations; i++)
            {
                // Create a copy to prevent bleeding
                Vector4[] oldColors = new Vector4[this.Points.Count];

                for (int j = 0; j < this.Points.Count; j++)
                {
                    oldColors[j] = this.Points[j].Color;
                }

                for (int j = 0; j < this.Points.Count; j++)
                {
                    if (j == 0 && includeEndpoints == true)
                    {
                        this.Points[j].Color = (oldColors[j] + oldColors[j + 1]) * .5f;
                    }

                    if (j != 0)
                    {
                        if (j != this.Points.Count - 1)
                        {
                            this.Points[j].Color = (oldColors[j - 1] + oldColors[j] + oldColors[j + 1]) * .33333333f;
                        }
                    }

                    if (j == this.Points.Count - 1 && includeEndpoints == true)
                    {
                        this.Points[j].Color = (oldColors[j - 1] + oldColors[j]) * .5f;
                    }
                }
            }
        }

        /// <summary>
        /// Smooths the Continuous Line
        /// </summary>
        /// <param name="iterations">Number of times to perform the smoothing operation</param>
        /// <param name="smoothingMethod">Method used for Smothing the Continuous Line</param>
        public void SmoothLine(int iterations, Enumerations.SmoothingMethod smoothingMethod)
        {
            switch (smoothingMethod)
            {
                case Enumerations.SmoothingMethod.Average:
                    this.SmoothPointsUsingAverage(iterations);
                    break;

                case Enumerations.SmoothingMethod.Gaussian:

                    break;

                case Enumerations.SmoothingMethod.Kernal:
                    break;
            }
        }

        /// <summary>
        /// Smooths our the Normals in this Continuous Line
        /// Normals are re Normalized after each iteration
        /// </summary>
        /// <param name="iterations">Number of times to perform smoothing operations</param>
        /// <param name="includeEndpoints">Should Endpoints be smoothed</param>
        public void SmoothNormalsUsingAverage(int iterations, bool includeEndpoints)
        {
            for (int i = 0; i < iterations; i++)
            {
                // Create a copy to prevent bleeding
                Vector3[] oldNormals = new Vector3[this.Points.Count];

                for (int j = 0; j < this.Points.Count; j++)
                {
                    oldNormals[j] = this.Points[j].Position;
                }

                for (int j = 0; j < this.Points.Count; j++)
                {
                    if (j == 0 && includeEndpoints == true)
                    {
                        this.Points[j].Normal = (oldNormals[j] + oldNormals[j + 1]) * .5f;
                    }

                    if (j != 0)
                    {
                        if (j != this.Points.Count - 1)
                        {
                            this.Points[j].Normal = (oldNormals[j - 1] + oldNormals[j] + oldNormals[j + 1]) * .33333333f;
                        }
                    }

                    if (j == this.Points.Count - 1 && includeEndpoints == true)
                    {
                        this.Points[j].Normal = (oldNormals[j - 1] + oldNormals[j]) * .5f;
                    }
                }

                this.Normalize();
            }
        }

        /// <summary>
        /// Smooths out he Points Positions, Normals, Colors, and width based on simple averaging
        /// </summary>
        /// <param name="iterations">Number of times to perform the Smooth operation</param>
        public void SmoothPointsUsingAverage(int iterations)
        {
            for (int i = 0; i < iterations; i++)
            {
                // Create a copy to avoid Bleeding
                Point[] oldPoints = new Point[this.Points.Count];

                for (int j = 0; j < this.Points.Count; j++)
                {
                    oldPoints[j] = new Point(this.Points[j]);
                }

                for (int j = 0; j < this.Points.Count; j++)
                {
                    if (j != 0 && j != this.Points.Count - 1)
                    {
                        this.Points[j].Position = (oldPoints[j - 1].Position + oldPoints[j].Position + oldPoints[j + 1].Position) * .33333333f;
                        this.Points[j].Normal = (oldPoints[j - 1].Normal + oldPoints[j].Normal + oldPoints[j + 1].Normal) * .33333333f;
                        this.Points[j].Color = (oldPoints[j - 1].Color + oldPoints[j].Color + oldPoints[j + 1].Color) * .33333333f;
                        this.Points[j].Width = (oldPoints[j - 1].Width + oldPoints[j].Width + oldPoints[j + 1].Width) * .33333333f;
                    }
                }

                this.Normalize();
            }
        }

        /// <summary>
        /// Smooths out the Points Positions using averaging
        /// </summary>
        /// <param name="iterations">Number oftimes to perform the Smooth Operation</param>
        /// <param name="includeEndpoints">True to smooth the first and last Positions</param>
        public void SmoothPositionsUsingAverage(int iterations, bool includeEndpoints)
        {
            for (int i = 0; i < iterations; i++)
            {
                // Create a copy to prevent bleeding
                Vector3[] oldPositions = new Vector3[this.Points.Count];

                for (int j = 0; j < this.Points.Count; j++)
                {
                    oldPositions[j] = this.Points[j].Position;
                }

                for (int j = 0; j < this.Points.Count; j++)
                {
                    if (j == 0 && includeEndpoints == true)
                    {
                        this.Points[j].Position = (oldPositions[j] + oldPositions[j + 1]) * .5f;
                    }

                    if (j != 0)
                    {
                        if (j != this.Points.Count - 1)
                        {
                            this.Points[j].Position = (oldPositions[j - 1] + oldPositions[j] + oldPositions[j + 1]) * .33333333f;
                        }
                    }

                    if (j == this.Points.Count - 1 && includeEndpoints == true)
                    {
                        this.Points[j].Position = (oldPositions[j - 1] + oldPositions[j]) * .5f;
                    }
                }
            }
        }

        /// <summary>
        /// Smoth out the Widths in this Continuous Line
        /// </summary>
        /// <param name="iterations">Number oftime to perform the Smothing Operation</param>
        /// <param name="includeEndpoints">Should the Endpoints be included</param>
        public void SmoothWidthsUsingAverage(int iterations, bool includeEndpoints)
        {
            for (int i = 0; i < iterations; i++)
            {
                // Create a copy to prevent bleeding
                float[] oldWidths = new float[this.Points.Count];

                for (int j = 0; j < this.Points.Count; j++)
                {
                    oldWidths[j] = this.Points[j].Width;
                }

                for (int j = 0; j < this.Points.Count; j++)
                {
                    if (j == 0 && includeEndpoints == true)
                    {
                        this.Points[j].Width = (oldWidths[j] + oldWidths[j + 1]) * .5f;
                    }

                    if (j != 0)
                    {
                        if (j != this.Points.Count - 1)
                        {
                            this.Points[j].Width = (oldWidths[j - 1] + oldWidths[j] + oldWidths[j + 1]) * .33333333f;
                        }
                    }

                    if (j == this.Points.Count - 1 && includeEndpoints == true)
                    {
                        this.Points[j].Width = (oldWidths[j - 1] + oldWidths[j]) * .5f;
                    }
                }
            }
        }

        /// <summary>
        /// Subdividing the Continuous Line will add linear interpolated points between existing point
        /// Then it will smooth the original points by averaging with the new points
        /// </summary>
        public void SubdivideLine()
        {
            List<Point> newPoints = new List<Point>();

            for (int i = 0; i < this.points.Count; i++)
            {
                if (i != this.points.Count - 1)
                {
                    newPoints.Add(this.points[i]);
                    newPoints.Add(this.GetSegmentMidPoint(i, i + 1));
                }
                else
                {
                    newPoints.Add(this.points[i]);
                }
            }

            for (int i = 0; i < newPoints.Count; i++)
            {
                if (i % 2 == 0 && i != 0 && i != newPoints.Count - 1)
                {
                    newPoints[i].Position = (newPoints[i - 1].Position + newPoints[i].Position + newPoints[i + 1].Position) * .33333333f;
                    newPoints[i].Normal = (newPoints[i - 1].Normal + newPoints[i].Normal + newPoints[i + 1].Normal) * .33333333f;
                    newPoints[i].Color = (newPoints[i - 1].Color + newPoints[i].Color + newPoints[i + 1].Color) * .33333333f;
                    newPoints[i].Width = (newPoints[i - 1].Width + newPoints[i].Width + newPoints[i + 1].Width) * .33333333f;
                }
            }

            this.points = newPoints;
        }

        /// <summary>
        /// Scales the Positions for each point in the Continuous Line by uniform value
        /// </summary>
        /// <param name="scale">Scale for all axis</param>
        public void UniformScale(float scale)
        {
            for (int i = 0; i < this.points.Count; i++)
            {
                this.Points[i].Position = Maths.Scale(this.Points[i].Position, scale);
            }
        }

        /// <summary>
        /// Do the two Line Segments intersect
        /// This is a heavy Method so it is early out checked by checking segment bounds
        /// </summary>
        /// <param name="continuousLineA">Reference to the Conitnuous Line Segment A is a member of</param>
        /// <param name="continuousLineB">Reference to the Conitnuous Line Segment B is a member of</param>
        /// <param name="segmentStartA">Start Index of first segment</param>
        /// <param name="segmentStartB">Start Index of second segment</param>
        /// <param name="intersectionInformation">Intersection Information for Segment A and Segment B</param>
        /// <returns>True if the two segments intersect</returns>
        private bool DoSegmentsIntersect(ContinuousLine continuousLineA, ContinuousLine continuousLineB, int segmentStartA, int segmentStartB, out IntersectionInformation intersectionInformation)
        {
            // Early Out Bounds Check
            SegmentSphericalBounds segmentABounds = new SegmentSphericalBounds(continuousLineA.points[segmentStartA].Position, continuousLineA.points[segmentStartA + 1].Position);
            SegmentSphericalBounds segmentBBounds = new SegmentSphericalBounds(continuousLineB.points[segmentStartB].Position, continuousLineB.points[segmentStartB + 1].Position);

            if (segmentABounds.DoesIntersectWith(segmentBBounds) == false)
            {
                intersectionInformation = new IntersectionInformation(continuousLineA, continuousLineB, segmentStartA, segmentStartB, 0.0f, 0.0f);
                return false;
            }

            bool doSegmentsIntersect = false;
            Vector3 directionA = Vector3.Normalize(continuousLineA.points[segmentStartA + 1].Position - continuousLineA.points[segmentStartA].Position);
            Vector3 directionB = Vector3.Normalize(continuousLineB.points[segmentStartB + 1].Position - continuousLineB.points[segmentStartB].Position);

            Vector3 directionAB = continuousLineB.points[segmentStartB].Position - continuousLineA.points[segmentStartA].Position;
            Vector3 crossDirections = Vector3.Cross(directionA, directionB);
            float crossDirectionMagnitude = crossDirections.LengthSquared();
            float dotDirectionABCrossAB = Math.Abs(Vector3.Dot(directionAB, crossDirections));

            Vector3 intersectionPosition;

            if (dotDirectionABCrossAB < 0.00001f && crossDirectionMagnitude > 0.00001f)
            {
                Vector3 crossDirectionABDirectionB = Vector3.Cross(directionAB, directionB);

                float s = Vector3.Dot(crossDirectionABDirectionB, crossDirections) / crossDirectionMagnitude;
                intersectionPosition = continuousLineA.points[segmentStartA].Position + (directionA * s);

                float segmentADistanceToIntersection = Vector3.Distance(continuousLineA.points[segmentStartA].Position, intersectionPosition);
                float segmentBDistanceToIntersection = Vector3.Distance(continuousLineB.points[segmentStartB].Position, intersectionPosition);
                float segmentALength = Vector3.Distance(continuousLineA.points[segmentStartA].Position, continuousLineA.points[segmentStartA + 1].Position);
                float segmentBLength = Vector3.Distance(continuousLineB.points[segmentStartB].Position, continuousLineB.points[segmentStartB + 1].Position);

                if (segmentALength >= segmentADistanceToIntersection && segmentBLength >= segmentBDistanceToIntersection)
                {
                    float segmentASampleDistance = continuousLineA.GetSampleDistanceFromPoint(segmentStartA);
                    float segmentBSampleDistance = continuousLineB.GetSampleDistanceFromPoint(segmentStartB);
                    float sampleDistanceA = segmentASampleDistance + (segmentADistanceToIntersection / continuousLineA.Length);
                    float sampleDistanceB = segmentBSampleDistance + (segmentBDistanceToIntersection / continuousLineB.Length);

                    intersectionInformation = new IntersectionInformation(continuousLineA, continuousLineB, segmentStartA, segmentStartB, sampleDistanceA, sampleDistanceB);
                    doSegmentsIntersect = true;
                }
                else
                {
                    intersectionInformation = new IntersectionInformation(continuousLineA, continuousLineB, segmentStartA, segmentStartB, 0.0f, 0.0f);
                }
            }
            else
            {
                intersectionInformation = new IntersectionInformation(continuousLineA, continuousLineB, segmentStartA, segmentStartB, 0.0f, 0.0f);
            }

            return doSegmentsIntersect;
        }

        /// <summary>
        /// Calculates the sample distance to a point
        /// </summary>
        /// <param name="point">Point to get distance to</param>
        /// <returns>float from 0 to 1</returns>
        private float GetSampleDistanceFromPoint(int point)
        {
            return this.LengthToPoint(point) / this.Length;
        }

        /// <summary>
        /// Gets the segments length as a percentage of the whole
        /// </summary>
        /// <param name="segmentStart">The start index of the segment</param>
        /// <returns>Value between 0 to 1</returns>
        private float GetSegmentLengthAsPercent(int segmentStart)
        {
            return Vector3.Distance(this.points[segmentStart].Position, this.points[segmentStart + 1].Position) / this.Length;
        }
    }
}